import Head from 'next/head'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Cannabis Resolution</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to Cannabis Resolution!
        </h1>

        <div className={styles.grid}>
          <a href="/charter" className={styles.card}>
            <h3>Charter &rarr;</h3>
            <p>What, Why, Who</p>
          </a>

          <a href="/partners" className={styles.card}>
            <h3>Partners &rarr;</h3>
            <p>With Support</p>
          </a>

          <a href="/apply" className={styles.card}>
            <h3>Applications &rarr;</h3>
            <p>Join the Resolution</p>
          </a>

          <a href="/tgr" className={styles.card}>
            <h3>The Great Resolution &rarr;</h3>
            <p>'Resolution' &gt; 'Reset'</p>
          </a>
        </div>
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          <img src="/vercel.svg" alt="Vercel Logo" className={styles.logo} />
        </a>
      </footer>
    </div>
  )
}
